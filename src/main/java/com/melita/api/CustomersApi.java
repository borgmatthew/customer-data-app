package com.melita.api;

import com.melita.service.Customer;
import com.melita.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomersApi {

    private final CustomerService customerService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerData create(@Valid @NotNull @RequestBody final CustomerData customerData) {
        final Customer customer = customerService.createCustomer(customerData);
        return CustomerData.builder()
                           .name(customer.getName())
                           .surname(customer.getSurname())
                           .accountStatus(customer.getAccountStatus())
                           .serviceType(customer.getServiceType())
                           .build();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CustomerData getCustomer(@PathVariable("id") final UUID id) {
        final Customer customer = customerService.getCustomer(id.toString());
        return CustomerData.builder()
                           .name(customer.getName())
                           .surname(customer.getSurname())
                           .accountStatus(customer.getAccountStatus())
                           .serviceType(customer.getServiceType())
                           .build();
    }
}
