package com.melita.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.melita.service.AccountStatus;
import com.melita.service.Customer;
import com.melita.service.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class CustomerData implements Customer {

    @JsonProperty("name")
    @Pattern(regexp = "[a-zA-Z]{1,20}", message = "Invalid name format")
    @NotNull(message = "name is required")
    private String name;

    @JsonProperty("surname")
    @Pattern(regexp = "[a-zA-Z]{1,20}", message = "Invalid surname format")
    @NotNull(message = "surname is required")
    private String surname;

    @JsonProperty("type")
    @NotNull(message = "type is required")
    private ServiceType serviceType;

    @JsonProperty("status")
    @NotNull(message = "status is required")
    private AccountStatus accountStatus;
}
