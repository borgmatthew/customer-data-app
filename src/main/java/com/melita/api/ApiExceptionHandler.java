package com.melita.api;

import com.melita.service.CustomerNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Collection;
import java.util.stream.Collectors;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Collection<Error>> handleMethodArgumentNotValidException(final MethodArgumentNotValidException exception) {
        final Collection<Error> errors = exception.getBindingResult()
                                       .getAllErrors()
                                       .stream()
                                       .map(e -> new Error(e.getDefaultMessage()))
                                       .collect(Collectors.toList());
        log.info("Validation exception", exception.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Error> handleMethodArgumentTypeMismatchExceptionException(final MethodArgumentTypeMismatchException exception) {
        log.info("Validation exception", exception.getMessage());
        return new ResponseEntity<>(new Error("Invalid parameter: " + exception.getParameter().getParameterName()), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> handleHttpMessageNotReadableException(final HttpMessageNotReadableException exception) {
        log.info("Parsing exception", exception.getMessage());
        return new ResponseEntity<>(new Error("Cannot parse request"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CustomerNotFoundException.class)
    public ResponseEntity<Error> handleCustomerNotFoundException() {
        return new ResponseEntity<>(new Error("The customer with the given ID does not exist"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleGenericException(final Exception exception) {
        log.error("An unhandled exception occurred", exception);
        return new ResponseEntity<>(new Error("Something went wrong. Please try again later."), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
