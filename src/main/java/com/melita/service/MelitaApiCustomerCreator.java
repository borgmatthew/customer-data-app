package com.melita.service;

import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.MelitaBackendApi;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Creates a customer by calling the Melita API
 */
@Component
@RequiredArgsConstructor
@Slf4j
public class MelitaApiCustomerCreator implements CustomerCreator {

    private final MelitaBackendApi melitaBackendApi;

    @Override
    public Customer create(final Customer customer) {
        final MelitaApiCustomerModel apiModel = MelitaApiCustomerModel.builder()
                                                                      .name(customer.getName())
                                                                      .surname(customer.getSurname())
                                                                      .accountStatus(customer.getAccountStatus())
                                                                      .serviceType(customer.getServiceType())
                                                                      .build();
        try {
            final MelitaApiCustomerModel melitaCustomer = melitaBackendApi.createCustomer(apiModel);
            return new CustomerImpl(melitaCustomer.getName(), melitaCustomer.getSurname(),
                    melitaCustomer.getServiceType(), melitaCustomer.getAccountStatus());
        } catch (final MelitaApiConnectionException | MelitaApiInvalidResponseException e) {
            log.error("Error when creating customer", e);
            throw new CustomerServiceException(e);
        }
    }
}
