package com.melita.service;

public interface Customer {

    String getName();

    String getSurname();

    ServiceType getServiceType();

    AccountStatus getAccountStatus();
}
