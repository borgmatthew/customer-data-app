package com.melita.service;

public class CustomerServiceException extends RuntimeException {
    CustomerServiceException(final Throwable e) {
        super(e);
    }
}
