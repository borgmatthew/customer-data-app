package com.melita.service;

@FunctionalInterface
interface CustomerCreator {

    /**
     * Creates and stores a customer
     * @param customer The {@link Customer} to create
     * @return The created customer
     */
    Customer create(Customer customer);
}
