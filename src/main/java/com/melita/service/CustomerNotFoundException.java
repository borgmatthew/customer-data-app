package com.melita.service;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CustomerNotFoundException extends RuntimeException {
    public CustomerNotFoundException(final Throwable e) {
        super(e);
    }
}
