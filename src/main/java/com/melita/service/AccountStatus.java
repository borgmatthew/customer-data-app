package com.melita.service;

public enum AccountStatus {
    ACTIVE,
    SUSPENDED,
    CEASED
}
