package com.melita.service.backend;

import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.models.MelitaApiCustomerModel;

import java.util.List;

/**
 * Defines all operations available through the Melita Backend API
 */
public interface MelitaBackendApi {

    /**
     * @param request The customer data to create
     * @return The created customer data
     * @throws MelitaApiConnectionException Thrown when something goes wrong when connecting with the backend api
     * @throws MelitaApiInvalidResponseException Thrown if the response does not contain any customer data
     */
    MelitaApiCustomerModel createCustomer(MelitaApiCustomerModel request)
            throws MelitaApiConnectionException, MelitaApiInvalidResponseException;

    /**
     * @return The lst of customers held in the backend service
     */
    List<String> getAllCustomers();

    /**
     * @param id The customer ID
     * @return The customer for the provided ID
     */
    MelitaApiCustomerModel getCustomer(String id);
}
