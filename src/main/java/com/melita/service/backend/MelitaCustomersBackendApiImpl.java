package com.melita.service.backend;

import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import com.melita.service.backend.models.MelitaApiCustomerResponse;
import com.melita.service.backend.models.MelitaApiGetAllCustomersResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MelitaCustomersBackendApiImpl implements MelitaBackendApi {

    private static final String CUSTOMERS_URL = "/customers";

    private final MelitaApiExecutor melitaApiExecutor;

    @Override
    public MelitaApiCustomerModel createCustomer(final MelitaApiCustomerModel request) {
        final ResponseEntity<MelitaApiCustomerResponse> response = melitaApiExecutor.execute(request, CUSTOMERS_URL,
                HttpMethod.PUT, MelitaApiCustomerResponse.class);

        expect200Response(response);
        expectResponseBody(response);
        return response.getBody().getMelitaApiCustomerModel();
    }

    @Override
    public List<String> getAllCustomers() {
        final ResponseEntity<MelitaApiGetAllCustomersResponse> response = melitaApiExecutor.execute(null, CUSTOMERS_URL,
                HttpMethod.GET, MelitaApiGetAllCustomersResponse.class);

        expect200Response(response);
        expectResponseBody(response);
        return response.getBody().getCustomerIdsResponse().getCustomers();
    }

    @Override
    public MelitaApiCustomerModel getCustomer(final String id) {
        final ResponseEntity<MelitaApiCustomerResponse> response = melitaApiExecutor.execute(null,
                CUSTOMERS_URL + '/' + id, HttpMethod.GET, MelitaApiCustomerResponse.class);

        expect200Response(response);
        expectResponseBody(response);
        return response.getBody().getMelitaApiCustomerModel();
    }

    private void expectResponseBody(final ResponseEntity response) {
        if (!response.hasBody()) {
            throw new MelitaApiInvalidResponseException("Expected data in response");
        }
    }

    private void expect200Response(final ResponseEntity response) {
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new MelitaApiInvalidResponseException("Expected a successful response from Melita API");
        }
    }

}
