package com.melita.service.backend.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MelitaApiGetAllCustomersResponse {

    @JsonProperty("data")
    private CustomerIdsResponse customerIdsResponse;
}
