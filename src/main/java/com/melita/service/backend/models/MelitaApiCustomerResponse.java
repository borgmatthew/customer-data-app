package com.melita.service.backend.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MelitaApiCustomerResponse {

    @JsonProperty("data")
    private MelitaApiCustomerModel melitaApiCustomerModel;
}
