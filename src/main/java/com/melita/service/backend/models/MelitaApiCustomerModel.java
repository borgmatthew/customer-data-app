package com.melita.service.backend.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.melita.service.AccountStatus;
import com.melita.service.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MelitaApiCustomerModel {

    @JsonProperty("name")
    private String name;

    @JsonProperty("surname")
    private String surname;

    @JsonProperty("type")
    private ServiceType serviceType;

    @JsonProperty("status")
    private AccountStatus accountStatus;
}
