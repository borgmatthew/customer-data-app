package com.melita.service.backend.exception;

public class MelitaApiConnectionException extends AbstractMelitaApiException {

    public MelitaApiConnectionException(final Throwable cause) {
        super(cause);
    }
}
