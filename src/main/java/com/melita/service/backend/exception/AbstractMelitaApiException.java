package com.melita.service.backend.exception;

public class AbstractMelitaApiException extends RuntimeException {

    public AbstractMelitaApiException() {
    }

    public AbstractMelitaApiException(final Throwable cause) {
        super(cause);
    }

    public AbstractMelitaApiException(final String message) {
        super(message);
    }
}
