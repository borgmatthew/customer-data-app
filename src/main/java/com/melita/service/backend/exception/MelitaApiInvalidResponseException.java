package com.melita.service.backend.exception;

public class MelitaApiInvalidResponseException extends AbstractMelitaApiException {

    public MelitaApiInvalidResponseException(final String message) {
        super(message);
    }
}
