package com.melita.service.backend;

import com.melita.service.backend.exception.AbstractMelitaApiException;
import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiNotFoundException;
import com.melita.service.backend.exception.MelitaApiUnhanledStatusCodeException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Responsible to execute API requests and wraps any errors that might occur.
 * Also takes care to add any required headers to the requests.
 */
@Component
class MelitaApiExecutor {

    private final RestTemplate restTemplate;
    private final String candidateId;
    private final String melitaUrl;
    private final Map<HttpStatus, Supplier<AbstractMelitaApiException>> httpStatusCodeMapper;

    MelitaApiExecutor(final RestTemplate restTemplate, @Value("${melita-api.candidate-id}") final String candidateId,
                      @Value("${melita-api.url}") final String melitaUrl) {
        this.restTemplate = restTemplate;
        this.candidateId = candidateId;
        this.melitaUrl = melitaUrl;
        httpStatusCodeMapper = new HashMap<>(1);
        httpStatusCodeMapper.put(HttpStatus.NOT_FOUND, MelitaApiNotFoundException::new);
    }

    <RESPONSE, REQUEST> ResponseEntity<RESPONSE> execute(final REQUEST request, final String resourceUrl, final HttpMethod method,
                                                         final Class<RESPONSE> responseClass) {
        try {
            return restTemplate.exchange(melitaUrl + resourceUrl, method, new HttpEntity<>(request, constructHeaders()), responseClass);
        } catch (final HttpClientErrorException httpClientErrorException) {
            final Supplier<AbstractMelitaApiException> exception = Optional.ofNullable(
                    httpStatusCodeMapper.get(httpClientErrorException.getStatusCode())).orElseGet(() -> MelitaApiUnhanledStatusCodeException::new);
            throw exception.get();
        } catch (final Exception e) {
            throw new MelitaApiConnectionException(e);
        }
    }

    private HttpHeaders constructHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.put("candidate-id", Collections.singletonList(candidateId));
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
