package com.melita.service;

import com.melita.service.backend.exception.AbstractMelitaApiException;
import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.MelitaBackendApi;
import com.melita.service.backend.exception.MelitaApiNotFoundException;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class MelitaApiCustomerRetriever implements CustomerRetriever {

    private final MelitaBackendApi melitaBackendApi;

    @Override
    public List<String> getAllCustomers() {
        try {
            return melitaBackendApi.getAllCustomers();
        } catch (final MelitaApiConnectionException | MelitaApiInvalidResponseException e) {
            log.error("Error while fetching all customers", e);
            throw new CustomerServiceException(e);
        }
    }

    @Override
    public Customer getCustomer(final String id) {
        try {
            final MelitaApiCustomerModel melitaCustomer = melitaBackendApi.getCustomer(id);
            return new CustomerImpl(melitaCustomer.getName(), melitaCustomer.getSurname(), melitaCustomer.getServiceType(), melitaCustomer.getAccountStatus());
        } catch (final MelitaApiNotFoundException notFound) {
            log.info(String.format("Customer with id [%s] not found!", id), notFound);
            throw new CustomerNotFoundException(notFound);
        } catch (final AbstractMelitaApiException e) {
            log.error("Error when creating customer", e);
            throw new CustomerServiceException(e);
        }
    }
}
