package com.melita.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class CustomerServiceImpl implements CustomerService {

    private final CustomerCreator customerCreator;
    private final CustomerRetriever customerRetriever;

    @Override
    public Customer createCustomer(final Customer customer) {
        return customerCreator.create(customer);
    }

    @Override
    public List<String> getAllCustomers() throws CustomerServiceException {
        return customerRetriever.getAllCustomers();
    }

    @Override
    public Customer getCustomer(final String id) throws CustomerServiceException {
        return customerRetriever.getCustomer(id);
    }
}
