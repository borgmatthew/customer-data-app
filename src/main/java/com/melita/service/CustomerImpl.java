package com.melita.service;

import lombok.Value;

@Value
class CustomerImpl implements Customer {
    String name;
    String surname;
    ServiceType serviceType;
    AccountStatus accountStatus;
}
