package com.melita.service;

import java.util.List;

/**
 * A service which is responsible for any operation peroformed on the customer data.
 */
public interface CustomerService {

    Customer createCustomer(Customer customer) throws CustomerServiceException;

    List<String> getAllCustomers() throws CustomerServiceException;

    Customer getCustomer(String id) throws CustomerServiceException;
}
