package com.melita.service;

import java.util.List;

interface CustomerRetriever {

    /**
     * @return List of customer IDs
     */
    List<String> getAllCustomers();

    Customer getCustomer(final String id);
}
