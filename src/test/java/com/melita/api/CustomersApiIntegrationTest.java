package com.melita.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.melita.service.AccountStatus;
import com.melita.service.Customer;
import com.melita.service.CustomerNotFoundException;
import com.melita.service.CustomerService;
import com.melita.service.ServiceType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomersApi.class)
class CustomersApiIntegrationTest {

    private static final String CUSTOMERS_RESOURCE = "/customers";
    private static final String INVALID_SURNAME_ERROR = "Invalid surname format";
    private static final String INVALID_NAME_ERROR = "Invalid name format";
    private static final String NAME = "matthew";
    private static final String SURNAME = "borg";
    private static final String CUSTOMER_ID = UUID.randomUUID().toString();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CustomerService customerService;

    @Test
    void create_validRequest_returnsCustomerResponse() throws Exception {
        final TestCustomer validCustomerRequest = TestCustomer.builder()
                                                              .name(NAME)
                                                              .surname(SURNAME)
                                                              .accountStatus(TestAccountStatus.ACTIVE)
                                                              .serviceType(TestServiceType.BUSINESS)
                                                              .build();
        when(customerService.createCustomer(any(Customer.class))).thenReturn(getCustomer());

        final MockHttpServletResponse response = mockMvc.perform(
                post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(validCustomerRequest)))
                                                        .andExpect(status().isOk())
                                                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                                                        .andReturn()
                                                        .getResponse();
        final TestCustomer createdCustomer = objectMapper.readValue(response.getContentAsString(), TestCustomer.class);
        assertThat(createdCustomer).isEqualTo(validCustomerRequest);
    }

    private Customer getCustomer() {
        return new Customer() {
            @Override
            public String getName() {
                return NAME;
            }

            @Override
            public String getSurname() {
                return SURNAME;
            }

            @Override
            public ServiceType getServiceType() {
                return ServiceType.BUSINESS;
            }

            @Override
            public AccountStatus getAccountStatus() {
                return AccountStatus.ACTIVE;
            }
        };
    }

    @Test
    void create_nameNotProvided_returns400Response() throws Exception {
        final TestCustomer customerWithNoName = TestCustomer.builder()
                                                            .surname(SURNAME)
                                                            .accountStatus(TestAccountStatus.ACTIVE)
                                                            .serviceType(TestServiceType.BUSINESS)
                                                            .build();

        mockMvc.perform(post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(customerWithNoName)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("name is required")));
    }

    @Test
    void create_nameContainsNumbers_returns400Response() throws Exception {
        final TestCustomer customerWithInvalidName = TestCustomer.builder()
                                                                 .name("123456")
                                                                 .surname(SURNAME)
                                                                 .accountStatus(TestAccountStatus.ACTIVE)
                                                                 .serviceType(TestServiceType.BUSINESS)
                                                                 .build();

        mockMvc.perform(
                post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(customerWithInvalidName)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("Invalid name format")));
    }

    @Test
    void create_surnameContainsNumbers_returns400Response() throws Exception {
        final TestCustomer customerWithInvalidSurname = TestCustomer.builder()
                                                                    .name("matthew")
                                                                    .surname("123456")
                                                                    .accountStatus(TestAccountStatus.ACTIVE)
                                                                    .serviceType(TestServiceType.BUSINESS)
                                                                    .build();

        mockMvc.perform(
                post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(customerWithInvalidSurname)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("Invalid surname format")));
    }

    @Test
    void create_surnameNotProvided_returns400Response() throws Exception {
        final TestCustomer customerWithoutSurname = TestCustomer.builder()
                                                                .name("matthew")
                                                                .accountStatus(TestAccountStatus.ACTIVE)
                                                                .serviceType(TestServiceType.BUSINESS)
                                                                .build();

        mockMvc.perform(
                post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(customerWithoutSurname)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("surname is required")));
    }

    @Test
    void create_nameAndSurnameInvalid_returns400ResponseWithMultipleErrors() throws Exception {
        final TestCustomer customerWithInvalidNameAndSurname = TestCustomer.builder()
                                                                           .name("123987")
                                                                           .surname("123456")
                                                                           .accountStatus(TestAccountStatus.ACTIVE)
                                                                           .serviceType(TestServiceType.BUSINESS)
                                                                           .build();

        final MockHttpServletResponse response = mockMvc.perform(post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON)
                                                                                         .content(objectMapper.writeValueAsBytes(
                                                                                                 customerWithInvalidNameAndSurname)))
                                                        .andExpect(status().isBadRequest())
                                                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                                                        .andReturn()
                                                        .getResponse();
        final List<TestError> errors = objectMapper.readValue(response.getContentAsString(), new TypeReference<>() {
        });
        assertThat(errors).hasSize(2);
        assertThat(errors).containsExactlyInAnyOrder(new TestError(INVALID_SURNAME_ERROR), new TestError(INVALID_NAME_ERROR));
    }

    @Test
    void create_missingServiceType_returns400Response() throws Exception {
        final TestCustomer customerWithoutServiceType = TestCustomer.builder()
                                                                    .name("matthew")
                                                                    .surname(SURNAME)
                                                                    .accountStatus(TestAccountStatus.ACTIVE)
                                                                    .build();

        mockMvc.perform(
                post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsBytes(customerWithoutServiceType)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("type is required")));
    }

    @Test
    void create_missingAccountStatus_returns400Response() throws Exception {
        final TestCustomer customerWithoutAccountStatus = TestCustomer.builder()
                                                                      .name("matthew")
                                                                      .surname(SURNAME)
                                                                      .serviceType(TestServiceType.BUSINESS)
                                                                      .build();

        mockMvc.perform(post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON)
                                                .content(objectMapper.writeValueAsBytes(customerWithoutAccountStatus)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("[0].message", equalTo("status is required")));
    }

    @Test
    void create_invalidServiceType_returns400Response() throws Exception {
        final TestCustomer customerWithInvalidServiceType = TestCustomer.builder()
                                                                        .name("matthew")
                                                                        .surname(SURNAME)
                                                                        .accountStatus(TestAccountStatus.ACTIVE)
                                                                        .serviceType(TestServiceType.INVALID)
                                                                        .build();

        mockMvc.perform(post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON)
                                                .content(objectMapper.writeValueAsBytes(customerWithInvalidServiceType)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("message", equalTo("Cannot parse request")));
    }

    @Test
    void create_invalidAccountStatus_returns400Response() throws Exception {
        final TestCustomer customerWithInvalidAccountStatus = TestCustomer.builder()
                                                                          .name("matthew")
                                                                          .surname(SURNAME)
                                                                          .accountStatus(TestAccountStatus.INVALID)
                                                                          .serviceType(TestServiceType.BUSINESS)
                                                                          .build();

        mockMvc.perform(post(CUSTOMERS_RESOURCE).contentType(MediaType.APPLICATION_JSON)
                                                .content(objectMapper.writeValueAsBytes(customerWithInvalidAccountStatus)))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("message", equalTo("Cannot parse request")));
    }

    @Test
    void getCustomer_customerDoesNotExist_returns404Response() throws Exception {
        when(customerService.getCustomer(CUSTOMER_ID)).thenThrow(new CustomerNotFoundException());

        mockMvc.perform(get(CUSTOMERS_RESOURCE + '/' + CUSTOMER_ID).contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("message", equalTo("The customer with the given ID does not exist")));
    }

    @Test
    void getCustomer_incorrectIdFormat_returns400Response() throws Exception {
        mockMvc.perform(get(CUSTOMERS_RESOURCE + '/' + "12d*").contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("message", equalTo("Invalid parameter: id")));
    }
}
