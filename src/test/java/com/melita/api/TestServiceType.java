package com.melita.api;

public enum TestServiceType {
    REGULAR,
    BUSINESS,
    INVALID
}
