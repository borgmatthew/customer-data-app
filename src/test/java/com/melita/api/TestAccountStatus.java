package com.melita.api;

public enum TestAccountStatus {
    ACTIVE,
    SUSPENDED,
    CEASED,
    INVALID
}
