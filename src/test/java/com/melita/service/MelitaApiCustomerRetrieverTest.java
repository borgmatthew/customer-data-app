package com.melita.service;

import com.melita.service.backend.MelitaBackendApi;
import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.exception.MelitaApiNotFoundException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MelitaApiCustomerRetrieverTest {
    private static final String CUSTOMER_ID = "123";

    private final MelitaBackendApi melitaBackendApi = mock(MelitaBackendApi.class);
    private final MelitaApiCustomerRetriever customerRetriever = new MelitaApiCustomerRetriever(melitaBackendApi);

    @Test
    void getAllCustomers_apiThrowsMelitaApiConnectionException_throwsCustomerServiceException() {
        when(melitaBackendApi.getAllCustomers()).thenThrow(new MelitaApiConnectionException(new IllegalStateException()));

        assertThatThrownBy(customerRetriever::getAllCustomers).isInstanceOf(CustomerServiceException.class);
    }

    @Test
    void getAllCustomers_apiThrowsMelitaInvalidResponseException_throwsCustomerServiceException() {
        when(melitaBackendApi.getAllCustomers()).thenThrow(new MelitaApiInvalidResponseException(""));

        assertThatThrownBy(customerRetriever::getAllCustomers).isInstanceOf(CustomerServiceException.class);
    }

    @Test
    void getCustomer_apiThrowsMelitaApiNotFoundException_throwsCustomerNotFoundException() {
        when(melitaBackendApi.getCustomer(CUSTOMER_ID)).thenThrow(new MelitaApiNotFoundException());

        assertThatThrownBy(() -> customerRetriever.getCustomer(CUSTOMER_ID)).isInstanceOf(CustomerNotFoundException.class);
    }

    @Test
    void getCustomer_apiThrowsMelitaApiConnectionException_throwsCustomerServiceException() {
        when(melitaBackendApi.getCustomer(CUSTOMER_ID)).thenThrow(new MelitaApiConnectionException(new IllegalStateException()));

        assertThatThrownBy(() -> customerRetriever.getCustomer(CUSTOMER_ID)).isInstanceOf(CustomerServiceException.class);
    }

    @Test
    void getCustomer_apiThrowsMelitaInvalidResponseException_throwsCustomerServiceException() {
        when(melitaBackendApi.getCustomer(CUSTOMER_ID)).thenThrow(new MelitaApiInvalidResponseException(""));

        assertThatThrownBy(() -> customerRetriever.getCustomer(CUSTOMER_ID)).isInstanceOf(CustomerServiceException.class);
    }

}