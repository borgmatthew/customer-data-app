package com.melita.service.backend;

import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.models.MelitaApiCustomerResponse;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import com.melita.service.backend.models.MelitaApiGetAllCustomersResponse;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MelitaCustomersBackendApiImplTest {

    private final MelitaApiExecutor apiExecutor = mock(MelitaApiExecutor.class);
    private final MelitaCustomersBackendApiImpl backendApi = new MelitaCustomersBackendApiImpl(apiExecutor);

    @Test
    void createCustomer_noResponseBody_throwsMelitaApiInvalidResponseException() {
        final MelitaApiCustomerModel request = new MelitaApiCustomerModel();
        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(HttpStatus.OK);

        when(apiExecutor.execute(eq(request), eq("/customers"), eq(HttpMethod.PUT),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        assertThatThrownBy(() -> backendApi.createCustomer(request)).isInstanceOf(
                MelitaApiInvalidResponseException.class).hasMessage("Expected data in response");
    }

    @Test
    void createCustomer_responseIsNot200_throwsMelitaApiInvalidResponseException() {
        final MelitaApiCustomerModel request = new MelitaApiCustomerModel();
        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(
                new MelitaApiCustomerResponse(), HttpStatus.BAD_REQUEST);

        when(apiExecutor.execute(eq(request), eq("/customers"), eq(HttpMethod.PUT),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        assertThatThrownBy(() -> backendApi.createCustomer(request)).isInstanceOf(
                MelitaApiInvalidResponseException.class).hasMessage("Expected a successful response from Melita API");
    }

    @Test
    void getAllCustomers_noResponseBody_throwsMelitaApiInvalidResponseException() {
        final ResponseEntity<MelitaApiGetAllCustomersResponse> response = new ResponseEntity<>(HttpStatus.OK);

        when(apiExecutor.execute(ArgumentMatchers.isNull(), eq("/customers"), eq(HttpMethod.GET),
                eq(MelitaApiGetAllCustomersResponse.class))).thenReturn(response);

        assertThatThrownBy(backendApi::getAllCustomers).isInstanceOf(MelitaApiInvalidResponseException.class)
                                                       .hasMessage("Expected data in response");
    }

    @Test
    void getAllCustomers_responseIsNot200_throwsMelitaApiInvalidResponseException() {
        final ResponseEntity<MelitaApiGetAllCustomersResponse> response = new ResponseEntity<>(
                new MelitaApiGetAllCustomersResponse(), HttpStatus.BAD_REQUEST);

        when(apiExecutor.execute(ArgumentMatchers.isNull(), eq("/customers"), eq(HttpMethod.GET),
                eq(MelitaApiGetAllCustomersResponse.class))).thenReturn(response);

        assertThatThrownBy(backendApi::getAllCustomers).isInstanceOf(MelitaApiInvalidResponseException.class)
                                                       .hasMessage("Expected a successful response from Melita API");
    }

    @Test
    void getCustomer_noResponseBody_throwsMelitaApiInvalidResponseException() {
        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(HttpStatus.OK);

        when(apiExecutor.execute(ArgumentMatchers.isNull(), eq("/customers/123"), eq(HttpMethod.GET),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        assertThatThrownBy(() -> backendApi.getCustomer("123")).isInstanceOf(MelitaApiInvalidResponseException.class)
                                                       .hasMessage("Expected data in response");
    }

    @Test
    void getCustomer_responseIsNot200_throwsMelitaApiInvalidResponseException() {
        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(new MelitaApiCustomerResponse(), HttpStatus.ACCEPTED);

        when(apiExecutor.execute(ArgumentMatchers.isNull(), eq("/customers/123"), eq(HttpMethod.GET),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        assertThatThrownBy(() -> backendApi.getCustomer("123")).isInstanceOf(MelitaApiInvalidResponseException.class)
                                                       .hasMessage("Expected a successful response from Melita API");
    }
}