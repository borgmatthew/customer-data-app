package com.melita.service.backend;

import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiNotFoundException;
import com.melita.service.backend.exception.MelitaApiUnhanledStatusCodeException;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MelitaApiExecutorTest {

    private static final String MELITA_URL = "test";
    private static final String CANDIDATE_ID = "123456";

    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final MelitaApiExecutor melitaApiExecutor = new MelitaApiExecutor(restTemplate, CANDIDATE_ID, MELITA_URL);

    @Test
    void execute_genericExceptionOccurs_throwsMelitaApiConnectionException() {
        final RuntimeException runtimeException = new RuntimeException("Some exception");
        when(restTemplate.exchange(anyString(), eq(HttpMethod.PUT), any(HttpEntity.class), eq(Object.class))).thenThrow(runtimeException);

        assertThatThrownBy(() -> melitaApiExecutor.execute(new Object(), "", HttpMethod.PUT, Object.class)).isInstanceOf(
                MelitaApiConnectionException.class).hasCause(runtimeException);
    }

    @Test
    void execute_notFoundHttpClientErrorExceptionExceptionOccurs_throwsMelitaApiNotFoundException() {
        final HttpClientErrorException notFoundException = new HttpClientErrorException(HttpStatus.NOT_FOUND);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(Object.class))).thenThrow(notFoundException);

        assertThatThrownBy(() -> melitaApiExecutor.execute(null, "", HttpMethod.GET, Object.class)).isInstanceOf(MelitaApiNotFoundException.class);
    }

    @Test
    void execute_badRequestHttpClientErrorExceptionExceptionOccurs_throwsMelitaApiUnhandledStatusCodeException() {
        final HttpClientErrorException notFoundException = new HttpClientErrorException(HttpStatus.BAD_REQUEST);
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(Object.class))).thenThrow(notFoundException);

        assertThatThrownBy(() -> melitaApiExecutor.execute(null, "", HttpMethod.GET, Object.class)).isInstanceOf(
                MelitaApiUnhanledStatusCodeException.class);
    }

    @Test
    void execute_successfulCall_verifyCandidateIdHeaderIsSent() {
        final ArgumentCaptor<HttpEntity> argumentCaptor = ArgumentCaptor.forClass(HttpEntity.class);

        melitaApiExecutor.execute(new Object(), "", HttpMethod.PUT, Object.class);

        verify(restTemplate).exchange(eq(MELITA_URL), eq(HttpMethod.PUT), argumentCaptor.capture(), eq(Object.class));
        final HttpEntity submittedEntity = argumentCaptor.getValue();
        assertThat(submittedEntity.getHeaders()).isNotNull();
        assertThat(submittedEntity.getHeaders().get("candidate-id")).isNotNull().containsExactly(CANDIDATE_ID);
        assertThat(submittedEntity.getHeaders().getAccept()).containsExactly(MediaType.APPLICATION_JSON);
    }
}
