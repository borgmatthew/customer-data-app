package com.melita.service.backend;

import com.melita.service.AccountStatus;
import com.melita.service.CustomerService;
import com.melita.service.ServiceType;
import com.melita.service.backend.models.CustomerIdsResponse;
import com.melita.service.backend.models.MelitaApiCustomerResponse;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import com.melita.service.backend.models.MelitaApiGetAllCustomersResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {CustomerService.class, MelitaCustomersBackendApiImpl.class, MelitaApiExecutor.class})
class MelitaCustomersBackendApiImplIntegrationTest {

    private static final String NAME = "matthew";
    private static final String SURNAME = "borg";
    private static final ServiceType SERVICE_TYPE = ServiceType.REGULAR;
    private static final AccountStatus ACCOUNT_STATUS = AccountStatus.ACTIVE;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private MelitaCustomersBackendApiImpl melitaCustomersBackendApi;

    @Test
    void createCustomer_successfulResponse_returnsWhatApiGivesUs() {
        final String adjustedName = NAME + "something";
        final MelitaApiCustomerModel customerModel = MelitaApiCustomerModel.builder()
                                                                           .name(adjustedName)
                                                                           .surname(SURNAME)
                                                                           .accountStatus(ACCOUNT_STATUS)
                                                                           .serviceType(SERVICE_TYPE)
                                                                           .build();
        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(
                new MelitaApiCustomerResponse(customerModel), HttpStatus.OK);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.PUT), any(HttpEntity.class),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        final MelitaApiCustomerModel request = MelitaApiCustomerModel.builder()
                                                                     .name(NAME)
                                                                     .surname(SURNAME)
                                                                     .serviceType(SERVICE_TYPE)
                                                                     .accountStatus(ACCOUNT_STATUS)
                                                                     .build();
        final MelitaApiCustomerModel createdCustomer = melitaCustomersBackendApi.createCustomer(request);

        assertThat(createdCustomer).isEqualTo(customerModel);
    }

    @Test
    void getAllCustomers_successfulResponse_returnsWhatApiGivesUs() {
        final List<String> customerIds = Collections.singletonList("123456");

        final ResponseEntity<MelitaApiGetAllCustomersResponse> response = new ResponseEntity<>(
                new MelitaApiGetAllCustomersResponse(new CustomerIdsResponse(customerIds)), HttpStatus.OK);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                eq(MelitaApiGetAllCustomersResponse.class))).thenReturn(response);

        final List<String> allCustomerIds = melitaCustomersBackendApi.getAllCustomers();

        assertThat(allCustomerIds).isEqualTo(customerIds);
    }

    @Test
    void getCustomer_successfulResponse_returnsWhatApiGivesUs() {
        final MelitaApiCustomerModel customerModel = MelitaApiCustomerModel.builder()
                                                                           .name(NAME)
                                                                           .surname(SURNAME)
                                                                           .accountStatus(ACCOUNT_STATUS)
                                                                           .serviceType(SERVICE_TYPE)
                                                                           .build();
        final String customerId = "545465";

        final ResponseEntity<MelitaApiCustomerResponse> response = new ResponseEntity<>(
                new MelitaApiCustomerResponse(customerModel), HttpStatus.OK);

        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                eq(MelitaApiCustomerResponse.class))).thenReturn(response);

        final MelitaApiCustomerModel customer = melitaCustomersBackendApi.getCustomer(customerId);

        assertThat(customer).isEqualTo(customerModel);
    }
}