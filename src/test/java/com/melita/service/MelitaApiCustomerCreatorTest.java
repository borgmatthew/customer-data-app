package com.melita.service;

import com.melita.service.backend.exception.MelitaApiConnectionException;
import com.melita.service.backend.exception.MelitaApiInvalidResponseException;
import com.melita.service.backend.MelitaBackendApi;
import com.melita.service.backend.models.MelitaApiCustomerModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MelitaApiCustomerCreatorTest {

    private static final String NAME = "matthew";
    private static final String SURNAME = "borg";
    private static final ServiceType SERVICE_TYPE = ServiceType.REGULAR;
    private static final AccountStatus ACCOUNT_STATUS = AccountStatus.ACTIVE;

    private final MelitaBackendApi melitaBackendApi = mock(MelitaBackendApi.class);
    private final MelitaApiCustomerCreator customerCreator = new MelitaApiCustomerCreator(melitaBackendApi);

    @Test
    void create_apiThrowsMelitaApiConnectionException_throwsCustomerServiceException() {
        when(melitaBackendApi.createCustomer(any(MelitaApiCustomerModel.class))).thenThrow(
                new MelitaApiConnectionException(new IllegalStateException()));

        assertThatThrownBy(() -> customerCreator.create(
                new CustomerImpl(NAME, SURNAME, SERVICE_TYPE, ACCOUNT_STATUS))).isInstanceOf(
                CustomerServiceException.class);
    }

    @Test
    void create_apiThrowsMelitaInvalidResponseException_throwsCustomerServiceException() {
        when(melitaBackendApi.createCustomer(any(MelitaApiCustomerModel.class))).thenThrow(
                new MelitaApiInvalidResponseException(""));

        assertThatThrownBy(() -> customerCreator.create(
                new CustomerImpl(NAME, SURNAME, SERVICE_TYPE, ACCOUNT_STATUS))).isInstanceOf(
                CustomerServiceException.class);
    }
}